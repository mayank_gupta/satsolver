#ifndef STATE_H
#define STATE_H

class State
{
    public:
        State(int size);
        virtual ~State();
        int GetSize ();
        void SetVariable (int id, bool val);
        bool GetVariable (int id);
        void UniformCrossover (State *x, State* y);
        void Mutate (double flipProbability);
        void FlipVariable (int id);
        void SetScore (int s) {m_score = s;};
        int GetScore () {return m_score;};
        void print ();
    protected:
    private:
        bool *m_pVariables;
        int m_size;
        int m_score;
};

#endif // STATE_H
