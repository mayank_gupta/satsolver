#ifndef CLAUSE_H
#define CLAUSE_H
#include <vector>
#include "State.h"
class Clause
{
    public:
        Clause(int s);
        virtual ~Clause();
        void AddVariable (int number);
        bool Satisfy (State *s);
        void print();
    protected:
    private:
        int m_size;
        std::vector<int> m_assignment;
};

#endif // CLAUSE_H
