#ifndef RANDOMNUMBERGENERATOR_H
#define RANDOMNUMBERGENERATOR_H


class RandomNumberGenerator
{
    public:
        static RandomNumberGenerator* Instance();
        int GetRandom (int range);
    protected:
    private:
        RandomNumberGenerator();
        RandomNumberGenerator(RandomNumberGenerator const &){};
        RandomNumberGenerator& operator=(RandomNumberGenerator const&) {};
        virtual ~RandomNumberGenerator();
        static RandomNumberGenerator* m_pInstance;
};

#endif // RANDOMNUMBERGENERATOR_H
