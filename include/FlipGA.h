#ifndef FLIPGA_H
#define FLIPGA_H
#include "State.h"
#include "Clause.h"
#include <vector>
#include <string>
class FlipGA
{
    public:
        FlipGA(std::string filename);
        virtual ~FlipGA();
        void PrintStates();
        void PrintClauses();
        void SetPopulationScore();
        void Iterate();
        void SelectCrossoverCandidates (int total, int &a, int&b);
        void Run();
        void SetMutateProbability(int p) {m_mutateProbability = p;};
        void PrintAssignment (bool b) {m_bPrintAssignment = b;};
        void PrintPopulation (bool b) {m_bPrintPopulation = b;}
        int GetVariables() {return m_variables;}
        int GetClauses() {return m_clauses;}
        double GetExecutionTime() {return m_runtime;}
        int GetFlips () {return m_Flips;}
        int GetIterations () {return m_Iterations;}
        void PrintBestAssignment ();
        void SetCutOff (int n) {m_cutoff = n;}
        State GetBestState ();
        bool IsGoalState (State s);
    protected:
    private:
        void LoadFromDisk (std::string filename);
        void FlipHeuristics (int n);
        int GetSatisfiedClauses (int n);
        std::vector<State> m_pPreviousPopulation, m_pCurrentPopulation;
        void ChangePopulation ();
        std::vector<Clause> m_pClauses;
        int m_variables,  m_clauses,  m_cnf;
        void SortPopulation ();
        void InitPopulation ();
        int m_NumberOfStates;
        int m_totalScore;
        int m_mutateProbability, m_Flips, m_Iterations;
        double m_runtime;
        bool m_bPrintAssignment, m_bPrintPopulation;
        int m_cutoff;
};

#endif // FLIPGA_H
