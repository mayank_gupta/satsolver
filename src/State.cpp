#include "State.h"
#include "RandomNumberGenerator.h"
#include <iostream>
State::State(int size): m_size(size)
{
    m_pVariables = new bool[size];
    for (int i=0; i< m_size; i++)
    {
        m_pVariables[i] = RandomNumberGenerator::Instance()->GetRandom(10) < 5 ? true : false;
    }
    m_score = -1;
    //ctor
}

State::~State()
{
    //dtor
}

int State::GetSize ()
{
    return m_size;
}
void State::SetVariable (int id, bool val)
{
    m_pVariables[id] = val;
}
void State::FlipVariable (int id)
{
    m_pVariables[id] = !m_pVariables[id];
}
bool State::GetVariable (int id)
{
    return m_pVariables[id];
}

void State::UniformCrossover (State *x, State* y)
{
    for (int i=0; i < m_size; i++)
    {
        int prob = RandomNumberGenerator::Instance()->GetRandom(10);
        m_pVariables[i] = prob < 5 ? x->GetVariable(i) : y->GetVariable(i);
    }
}

void State::Mutate(double flipProbablity)
{
    for (int i=0; i < m_size ; i++)
    {
        int prob = RandomNumberGenerator::Instance()->GetRandom(10);
        m_pVariables[i] = prob < 5 ? !m_pVariables[i] : m_pVariables[i];
    }
}

void State::print ()
{
    using namespace std;
    for (int i=0; i < m_size; i++)
    {
        string val = m_pVariables[i] ? "true" : "false";
        cout << "(" << i+1 << "," << val << ")" << "\t";
    }
    cout << endl;
}
