#include "RandomNumberGenerator.h"
#include <stddef.h>
#include <cstdlib>
#include <ctime>
RandomNumberGenerator* RandomNumberGenerator::m_pInstance = NULL;

int RandomNumberGenerator::GetRandom(int range)
{
    return rand()%range;
}

RandomNumberGenerator* RandomNumberGenerator::Instance()
{
    if (!m_pInstance)
    {
        m_pInstance = new RandomNumberGenerator();
        srand(time(NULL));
    }
    return m_pInstance;
}

RandomNumberGenerator::RandomNumberGenerator()
{
    //ctor
}

RandomNumberGenerator::~RandomNumberGenerator()
{
    //dtor
}
