#include "Clause.h"
#include <iostream>
using namespace std;
Clause::Clause(int number): m_size(number)
{
    //ctor
}

Clause::~Clause()
{
    //dtor
}

void Clause::AddVariable (int number)
{
    if ((int)m_assignment.size() > m_size)
        throw "More variables then expected";
    m_assignment.push_back(number);
}

bool Clause::Satisfy(State *s)
{
    bool result = false;
    for (std::vector<int>::iterator it = m_assignment.begin() ; it!= m_assignment.end(); it++)
    {
        int variable = *it;
        //cout << "(v:" << variable;
        bool neg = variable < 0;
        variable = variable < 0 ? -variable: variable;
        bool truth = neg ? !s->GetVariable(variable-1): s->GetVariable(variable-1);

        result |= truth;
        if (result) break;
    }

    return result;
}

void Clause::print ()
{
    using namespace std;
    for (std::vector<int>::iterator it= m_assignment.begin();it != m_assignment.end() ; it++)
    {
        cout << *it << "\t";
    }
    cout << endl;
}
