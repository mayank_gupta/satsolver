#include "FlipGA.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <sstream>
#include <ctime>
#include "RandomNumberGenerator.h"
using namespace std;
struct Comp {
    inline bool operator () (State a, State b)
    {
        return a.GetScore() < b.GetScore();
    }
};
typedef struct Comp comp;

void FlipGA::LoadFromDisk(std::string filename)
{
    using namespace std;
    ifstream infile(filename.c_str());
    string line;
    if (infile.is_open())
    {
        //cout << "file opened" << endl;
        while (getline(infile, line))
        {
            if (line[0] == 'c')
                continue;
            if (line[0] == 'p')
            {
                //cout << line << endl;
                stringstream convertor(line.substr(5));

                convertor >> m_variables;
                convertor >> m_clauses;
                //cout << m_variables << " " << m_clauses << endl;
                m_NumberOfStates = 10;
                InitPopulation();
                for (int i=0; i < m_clauses; i++)
                {
                    getline(infile, line);
                    Clause obj(3);
                    stringstream convertor2(line);
                    int val = 0;
                    for (int j=0; j < 3; j++)
                    {
                        convertor2 >> val;
                        if (val ==0) continue;
                        obj.AddVariable(val);
                    }
                    m_pClauses.push_back(obj);
                }
            }
        }

    }
    else
    {
        throw "404";
    }
    infile.close();
}

FlipGA::FlipGA(std::string filename)
{
    m_bPrintAssignment = false;
    m_bPrintPopulation = false;
    m_mutateProbability = 1;
    m_cutoff = -1;
    m_Flips = 0;
    LoadFromDisk(filename);
    //ctor
}

FlipGA::~FlipGA()
{
    //dtor
}

void FlipGA::FlipHeuristics (int n)
{
    State *pState = &m_pCurrentPopulation[n];
    int clauses_before = GetSatisfiedClauses(n);
    for (int i=0; i < m_variables; i++)
    {
        pState->FlipVariable(i);
        int clauses_after = GetSatisfiedClauses(n);

        if (clauses_after < clauses_before)
            pState->FlipVariable(i);
        else
        {
            m_Flips++;
            clauses_before = clauses_after;
        }
    }
    pState->SetScore(clauses_before);
}

int FlipGA::GetSatisfiedClauses (int n)
{
    int counter = 0;
    State *pState = &m_pCurrentPopulation[n];
    for (int i=0; i < m_clauses; i++)
    {
        bool satisfies = m_pClauses[i].Satisfy(pState);

        if (satisfies) counter++;
    }
    return counter;
}

void FlipGA::InitPopulation ()
{
    for (int i=0; i<m_NumberOfStates; i++)
    {
        m_pCurrentPopulation.push_back(State(m_variables));
        m_pPreviousPopulation.push_back(State(m_variables));
    }
}

void FlipGA::SetPopulationScore ()
{
    int total = 0;
    for (int i=0; i < m_NumberOfStates; i++)
    {
        //FlipHeuristics(i);
        int score = GetSatisfiedClauses(i);
        m_pCurrentPopulation[i].SetScore(score);
        total += m_pCurrentPopulation[i].GetScore();
    }
    m_totalScore = total;
    SortPopulation();
}

void FlipGA::SelectCrossoverCandidates (int total, int &a, int&b)
{
    int x = RandomNumberGenerator::Instance()->GetRandom(total);
    int y = RandomNumberGenerator::Instance()->GetRandom(total);
    int current = 0;
    int index = 0;
    for (vector<State>::iterator it = m_pPreviousPopulation.begin(); it!=m_pPreviousPopulation.end(); it++)
    {
        int low = current;
        int high = current + (*it).GetScore();

        if (x >= low && x < high)
            a = index;
        if (y >= low && y < high)
            b = index;

        index++;
        current += (*it).GetScore();
    }

    if (a==-1)
        cout << "a:" << x;
    if (b==-1)
        cout << "b:" << y;
    //cout << " total:" << total << endl;
}

void FlipGA::Iterate()
{
    using namespace std;
    SortPopulation();
    ChangePopulation();
    m_pCurrentPopulation.push_back(m_pPreviousPopulation.back()); m_pPreviousPopulation.pop_back();
    m_pCurrentPopulation.push_back(m_pPreviousPopulation.back()); m_pPreviousPopulation.pop_back();
    int score_total = 0;
    for (vector<State>::iterator it = m_pPreviousPopulation.begin(); it!=m_pPreviousPopulation.end(); it++)
    {
        score_total += (*it).GetScore();
    }

    for (int i=0; i<8; i++)
    {
        int a=-1, b=-1;
        SelectCrossoverCandidates(score_total, a, b);
        if (a==-1 || b==-1)
        {
            cout << "Crossover:(" << a << ","<< b<<")"<<endl;
            throw "Something wrong";
        }
        //cout << "Crossover:(" << a << ","<< b<<")"<<endl;

        State s(m_variables);
        s.UniformCrossover(&m_pPreviousPopulation[a], &m_pPreviousPopulation[b]);

        int random = RandomNumberGenerator::Instance()->GetRandom(10);
        if (random < m_mutateProbability)
            s.Mutate(1);
        m_pCurrentPopulation.push_back(s);
    }
    SetPopulationScore();
    for (int i=0; i < 8; i++)
    {
        FlipHeuristics(i);
    }
    SortPopulation();
}

void FlipGA::Run()
{
    m_runtime = 0;
    SetPopulationScore();
    m_Iterations = 0;
    time_t start_time, end_time;

    time(&start_time);

    while (m_pCurrentPopulation.back().GetScore() != m_clauses && ((m_cutoff < 0) || (m_Iterations < m_cutoff)))
    {
        Iterate();
        m_Iterations++;
        if (m_bPrintPopulation)
        {
            cout << "Iteration: " <<  m_Iterations << endl;
            PrintStates();
            cout << endl;
        }
    }
    time(&end_time);
    m_runtime = difftime(end_time, start_time);

    if (m_bPrintAssignment)
        m_pCurrentPopulation.back().print();
}

void FlipGA::PrintBestAssignment ()
{
     m_pCurrentPopulation.back().print();
}

State FlipGA::GetBestState ()
{
    return m_pCurrentPopulation.back();
}

bool FlipGA::IsGoalState (State s)
{
    int counter = 0;
    State *pState = &s;
    for (int i=0; i < m_clauses; i++)
    {
        bool satisfies = m_pClauses[i].Satisfy(pState);

        if (satisfies) counter++;
    }
    return counter==m_clauses;

}

void FlipGA::ChangePopulation()
{
    m_pPreviousPopulation = m_pCurrentPopulation;
    m_pCurrentPopulation.clear();
}

void FlipGA::SortPopulation()
{
    std::sort(m_pCurrentPopulation.begin(), m_pCurrentPopulation.end(), comp());
}

void FlipGA::PrintStates()
{
    int index =0;
    for (vector<State>::iterator it = m_pCurrentPopulation.begin(); it!=m_pCurrentPopulation.end(); it++)
    {
        cout << "(" << index++ << ":" << (*it).GetScore() <<")" << endl;
    }
}
void FlipGA::PrintClauses()
{
    using namespace std;
    for (vector<Clause>::iterator it = m_pClauses.begin(); it != m_pClauses.end(); it++)
    {
        (*it).print();
    }
}
