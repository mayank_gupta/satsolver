#include <iostream>
#include <sstream>
#include "FlipGA.h"
#include "argvparser.h"
#include <stdio.h>
#include <fstream>
using namespace std;

int process_files(string in, string out, int cutoff)
{
    cout << in<< endl;
    ifstream infile(in.c_str());
    ofstream outfile(out.c_str());
    string line;
    if (infile.is_open())
    {
         while (getline(infile, line) && line.length() > 0)
        {
            cout <<line<<endl;
            FlipGA f(line);
            f.SetCutOff(cutoff);
            f.Run();
            State s = f.GetBestState();
            if (f.IsGoalState(s))
            {
                outfile<<"T\t"<<f.GetIterations() << "\t"<<f.GetFlips()<<"\t"<<f.GetExecutionTime()<<endl;
            }
            else
            {
                outfile<<"F\t"<<f.GetIterations() << "\t"<<f.GetFlips()<<"\t"<<f.GetExecutionTime()<<endl;
            }
        }
    }
    infile.close();
    outfile.close();
    return -1;
}
int main (int argc, char **argv)
{
    using namespace CommandLineProcessing;
    ArgvParser cmd;
    cmd.setHelpOption("h", "help", "Print this help page");
    cmd.defineOption("file", "CNF file name.", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("file", "f");
    cmd.defineOption("verbose", "Prints intermediate state scores.", ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("verbose", "v");
    cmd.defineOption("assignment", "Prints the final assignment.", ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("assignment", "a");
    cmd.defineOption("scores", "Prints the final population scores.", ArgvParser::NoOptionAttribute);
    cmd.defineOptionAlternative("scores", "s");
    cmd.defineOption("mutate", "Mutation probability.", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("mutate", "m");
    cmd.defineOption("cutoff", "Maximum number of iterations.", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("cutoff", "c");
    cmd.defineOption("dir", "File containing file names to process.", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("dir", "d");
    cmd.defineOption("out", "Output file to write.", ArgvParser::OptionRequiresValue);
    cmd.defineOptionAlternative("out", "o");

    int result = cmd.parse(argc, argv);
    if (result != ArgvParser::NoParserError)
    {
        cout << cmd.parseErrorDescription(result);
        return -1;
    }
    if (cmd.foundOption("dir"))
    {
        string dir= cmd.optionValue("dir");
        if (!cmd.foundOption("out")) {cout<<"Need output file name"<<endl; return -1;}
        string out = cmd.optionValue("out");
        int cutoff = 1;
        if (cmd.foundOption("cutoff"))
        {
            stringstream convertor(cmd.optionValue("cutoff"));
            convertor >> cutoff;
        }
        process_files(dir, out, cutoff);
        return -1;

    }
    string file = "";
    if (cmd.foundOption("file"))
    {
        file= cmd.optionValue("file");
    }
    else
    {
        cout << "File name mandatory. Use --help for help menu." << endl;
        return -1;
    }

    FlipGA fg(file);

    if (cmd.foundOption("mutate"))
    {
        double mutation;
        stringstream convertor(cmd.optionValue("mutate"));
        convertor >> mutation;
        if (mutation > 1.0f)
        {
            cout << "Setting mutation to max." << endl;
            mutation = 1.0f;
        }
        cout << "Mutation probability: " << mutation << endl;
        fg.SetMutateProbability(mutation*10);
    }
    if (cmd.foundOption("cutoff"))
    {
        int cutoff;
        stringstream convertor(cmd.optionValue("cutoff"));
        convertor >> cutoff;
        cout << "Cutoff value: " << cutoff << endl;
        fg.SetCutOff(cutoff);
    }
    if (cmd.foundOption("verbose"))
    {
        fg.PrintPopulation(true);
    }

    //fg.SetMutateProbability(1);
    fg.Run();
    if (cmd.foundOption("assignment"))
    {
        fg.PrintBestAssignment();
    }

    if (cmd.foundOption("scores"))
    {
        cout << "Population scores:" << endl;
        fg.PrintStates();
    }

    State s = fg.GetBestState();
    if (fg.IsGoalState(s))
    {
        cout << "Solution Found in " << fg.GetIterations() << " iterations. Flips: " << fg.GetFlips() ;// << endl;
        printf(" Time: %.2lf\n", fg.GetExecutionTime());
    }
    else
    {
        cout << "Cutoff value exceeded: " << fg.GetIterations() << " iterations. Flips: " << fg.GetFlips() ;// << endl;
        printf(" Time: %.2lf\n", fg.GetExecutionTime());
    }


    return -1;
}
